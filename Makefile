install-dotnet-ubuntu-16.04:
	sudo apt-get update
	sudo apt-get install -y wget
	wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb
	sudo dpkg -i packages-microsoft-prod.deb
	sudo apt-get install -y apt-transport-https
	sudo apt-get update
	sudo apt-get install -y dotnet-sdk-2.1
	rm -f packages-microsoft-prod.deb*
	
db:
	docker volume create dotnet-database || true
	docker network create dotnet || true
	docker rm -f  db dotnet-phpmyadmin || true
	docker run -d --name db --network=dotnet -e MYSQL_ROOT_PASSWORD="root" -v $(PWD)/database:/var/lib/mysql mysql:5.7.22
	docker run -d --name dotnet-phpmyadmin -p 5005:80 --network=dotnet phpmyadmin/phpmyadmin:edge
